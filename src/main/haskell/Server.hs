{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards   #-}

module Server where

import           Message                (Message (..), UserId, asString,
                                         message, seqNo)
import           Multimap

import           Control.Applicative
import           Control.Concurrent.STM
import           Control.Monad          (forM_, forever)
import           Data.Attoparsec.Text
import           Data.Function          (on)
import           Data.List              (insertBy)
import           Data.Map               (Map)
import qualified Data.Map               as Map
import qualified Data.Set               as Set
import qualified Data.Text.IO           as Text
import           System.IO



data Client = Client
  { clientId       :: UserId
  , clientHandle   :: Handle
  , clientSendChan :: TChan Message
  } deriving Eq

newClient :: UserId -> Handle -> STM Client
newClient userId handle = do
  c <- newTChan
  return (Client userId handle c)

sendMessage :: Client -> Message -> STM ()
sendMessage Client{..} =
  writeTChan clientSendChan

addClient :: Server -> Client -> STM ()
addClient Server{..} c = do
  modifyTVar' clients $ Map.alter update (clientId c)
  where
    update :: Maybe [Client] -> Maybe [Client]
    update mcs = case mcs of
      Just cs -> Just (c:cs)
      Nothing -> Just [c]

removeClient :: Server -> Client -> STM ()
removeClient Server{..} c = do
  modifyTVar' clients $ Map.alter update (clientId c)
  where
    update = fmap $ filter (/= c)

data Server = Server
  { clients       :: TVar (Map UserId [Client])
  , followers     :: TVar (Multimap UserId UserId)
  , expectedSeqNo :: TVar Int
  , buffer        :: TVar [Message]
  }

newServer :: IO Server
newServer = Server
  <$> newTVarIO Map.empty
  <*> newTVarIO Map.empty
  <*> newTVarIO 1
  <*> newTVarIO []


follow :: Server -> UserId -> UserId -> STM ()
follow Server{..} follower followee =
  modifyTVar followers $
  modify (Set.insert follower) followee

unfollow :: Server -> UserId -> UserId -> STM ()
unfollow Server{..} follower followee =
  modifyTVar followers $
  modify (Set.delete follower) followee

send :: Server -> Message -> UserId -> STM ()
send Server{..} msg followee = do
  mc <- Map.lookup followee <$> readTVar clients
  case mc of
    Just cs -> forM_ cs $ \c -> sendMessage c msg
    Nothing -> return ()

broadcast :: Server -> Message -> STM ()
broadcast Server{..} msg = do
  clientMap <- readTVar clients
  let cs = concat (Map.elems clientMap)
  forM_ cs $ \c -> sendMessage c msg


handleMessage :: Server -> Message -> STM ()
handleMessage srv@Server{..} m = do
  buf <- readTVar buffer
  expected <- readTVar expectedSeqNo
  let (todo, remaining) = splitBuffer buf expected
  forM_ todo $ \msg ->
    case msg of
      Follow _ from to -> do
        follow srv from to
        send srv msg to
      Unfollow _ from to -> unfollow srv from to
      Broadcast _ -> broadcast srv msg
      PrivMsg _ _ to -> send srv msg to
      StatusUpdate _ from -> do
        fs <- (Multimap.lookup from) <$> (readTVar followers)
        mapM_ (send srv msg) (Set.toList fs)
  writeTVar buffer remaining
  writeTVar expectedSeqNo (expected + length todo)
  where
    updated msgs = (insertBy (compare `on` seqNo) m msgs)
    accumulate (msg, e) (xs, ys) =
      if (seqNo msg) <= e
        then (msg:xs, ys)
        else (xs, msg:ys)
    splitBuffer buf expected  = foldr accumulate ([], []) (zip (updated buf) [expected..])


runServer :: Handle -> Server -> IO ()
runServer h srv = forever $ do
  line <- Text.hGetLine h
  case (parseOnly message line) of
    Left err  -> hPutStrLn h err
    Right msg -> atomically $ handleMessage srv msg

runClient :: Client -> IO ()
runClient Client{..} = forever $ do
  msg <- atomically $ readTChan clientSendChan
  Text.hPutStrLn clientHandle (asString msg)

