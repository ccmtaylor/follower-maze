{-# LANGUAGE OverloadedStrings #-}
module Message where

import           Control.Applicative
import           Data.Attoparsec.Text
import           Data.Text            (Text)
import qualified Data.Text            as Text

type UserId = Int
type SeqNo = Int

data Message = Follow SeqNo UserId UserId
             | Unfollow SeqNo UserId UserId
             | Broadcast SeqNo
             | PrivMsg SeqNo UserId UserId
             | StatusUpdate SeqNo UserId
              deriving (Eq, Show)

seqNo :: Message -> SeqNo
seqNo (Follow s _ _) = s
seqNo (Unfollow s _ _) = s
seqNo (Broadcast s) = s
seqNo (PrivMsg s _ _) = s
seqNo (StatusUpdate s _) = s

asString :: Message -> Text
asString msg = Text.intercalate "|" (map Text.pack (fields msg))
  where
    fields :: Message -> [String]
    fields (Follow s from to) =
      [(show s), "F", (show from), (show to)]
    fields (Unfollow s from to) =
      [(show s), "U", (show from), (show to)]
    fields (Broadcast s) = [(show s), "B"]
    fields (PrivMsg s from to) =
      [(show s), "P", (show from), (show to)]
    fields (StatusUpdate s from) =
      [(show s), "S", (show from)]

follow, unfollow, broadcast, privMsg, statusUpdate, message :: Parser Message
follow = Follow <$> signed decimal <* "|F|"
                <*> signed decimal <* "|"
                <*> signed decimal

unfollow = Unfollow <$> signed decimal <* "|U|"
                    <*> signed decimal <* "|"
                    <*> signed decimal

broadcast = Broadcast <$> signed decimal <* "|B"


privMsg = PrivMsg <$> signed decimal <* "|P|"
                  <*> signed decimal <* "|"
                  <*> signed decimal

statusUpdate = StatusUpdate <$> signed decimal <* "|S|"
                            <*> signed decimal

message = follow
          <|> unfollow
          <|> broadcast
          <|> privMsg
          <|> statusUpdate

messages :: Parser [Message]
messages = message `sepBy` endOfLine
