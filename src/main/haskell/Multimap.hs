module Multimap where

import           Data.Map   (Map)
import qualified Data.Map   as Map
import           Data.Maybe (fromMaybe)
import           Data.Set   (Set)
import qualified Data.Set   as Set
import           Prelude    hiding (lookup)


type Multimap k v = Map k (Set v)

lookup :: (Ord k) => k -> Multimap k v -> Set v
lookup = Map.findWithDefault Set.empty

(!) :: (Ord k) => Multimap k v -> k -> (Set v)
(!) = flip lookup

insert :: (Ord k, Ord v) =>
          k -> v -> Multimap k v -> Multimap k v
insert k v = Map.insertWith Set.union k (Set.singleton v)

modify :: (Ord k) =>
          (Set v -> Set v) -> k -> Multimap k v -> Multimap k v
modify f = Map.alter (Just . update)
  where
    update = f . fromMaybe Set.empty
