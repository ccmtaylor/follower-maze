use std::collections::{HashMap, HashSet};
use std::sync::mpsc;

use message;

pub enum Message {
    ClientConnected(mpsc::Sender<message::Message>, u64),
    ClientGone(u64),
    MessageReceived(message::Message),
}
pub struct Dispatcher {
    pub tx: mpsc::Sender<Message>,
    input: mpsc::Receiver<Message>,
    clients: HashMap<u64, mpsc::Sender<message::Message>>,
    followers: HashMap<u64, HashSet<u64>>,
}
impl Dispatcher {
    pub fn new() -> Dispatcher {
        let (tx, rx) = mpsc::channel();
        Dispatcher {
            tx: tx,
            input: rx,
            clients: HashMap::new(),
            followers: HashMap::new(),
        }
    }
    pub fn run(mut self) {
        for msg in &self.input {
            match msg {
                Message::ClientConnected(tx, id) => {
                    println!("client {} connected", id);
                    self.clients.insert(id, tx);
                },
                Message::ClientGone(id) => {
                    println!("client {} disconnected", id);
                    self.clients.remove(&id);
                },
                Message::MessageReceived(m) => {
                    match m.event {
                        message::Event::Broadcast =>
                            for id in self.clients.keys() {
                                &self.send(m, id);
                            },
                        message::Event::Follow(src, dst) => {
                            {
                                let dst_followers = self.followers.entry(dst).or_insert(HashSet::new());
                                dst_followers.insert(src);
                            }
                            &self.send(m, &dst);
                        },
                        message::Event::Unfollow(src, dst) =>
                            if let Some(fs) = self.followers.get_mut(&dst) {
                                fs.remove(&src);
                            },
                        message::Event::PrivMsg(_, dst) => {
                            &self.send(m, &dst);
                        },
                        message::Event::Status(src) => {
                            let empty = HashSet::with_capacity(0);
                            for dst in self.followers.get(&src).unwrap_or(&empty) {
                                &self.send(m, dst);
                            }
                        },
                    }
                }
            }
        }
        for (_, tx) in self.clients {
            drop(tx);
        }
    }

    fn send(&self, m: message::Message, dst: &u64) {
        if let Some(tx) = self.clients.get(dst) {
            // println!("sending {} to client {}", m, dst);
            if let Err(_) = tx.send(m) {
                println!("client {} is gone, removing", dst);
                self.tx.send(Message::ClientGone(*dst)).unwrap();
            }
        }
    }
}
