use std::fmt;
use std::cmp;

pub type UserId = u64;

#[derive(Debug, Eq, PartialEq, Clone, Copy)]
pub enum Event {
    Follow(UserId, UserId),
    Unfollow(UserId, UserId),
    Broadcast,
    PrivMsg(UserId, UserId),
    Status(UserId),
}

impl fmt::Display for Event {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            Event::Follow(src, dst) => write!(f, "F|{}|{}", src, dst),
            Event::Unfollow(src, dst) => write!(f, "U|{}|{}", src, dst),
            Event::Broadcast => write!(f, "B"),
            Event::PrivMsg(src, dst) => write!(f, "P|{}|{}", src, dst),
            Event::Status(src) => write!(f, "S|{}", src),
        }
    }
}

#[derive(Debug, Eq, PartialEq, Clone, Copy)]
pub struct Message {
    pub seq_no: u64,
    pub event: Event,
}

impl fmt::Display for Message {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}|{}", self.seq_no, self.event)
    }
}

// we reverse the ordering so that the max-heap turns into a min-heap.
impl cmp::Ord for Message {
    fn cmp(&self, other: &Message) -> cmp::Ordering {
        self.seq_no.cmp(&other.seq_no).reverse()
    }
}

impl cmp::PartialOrd for Message {
    fn partial_cmp(&self, other: &Message) -> Option<cmp::Ordering> {
        Some(self.cmp(other))
    }
}
