
use std::io;
use std::io::prelude::*;
use std::net::TcpStream;
use std::num;
use std::sync::mpsc;

use message;

#[derive(Debug)]
pub enum ClientError {
    Io(io::Error),
    ClientId(num::ParseIntError),
    Other,
}

pub struct Client<W: Write> {
    pub id: u64,
    out: W,
    events: mpsc::Receiver<message::Message>,
}

impl<W: Write> Client<W> {
    pub fn new(chan: TcpStream, events: mpsc::Receiver<message::Message>) -> Result<Client<TcpStream>, ClientError> {
        let client_id = {
            let mut br = io::BufReader::new(&chan);
            let mut buf = String::new();
            try!(br.read_line(&mut buf).map_err(ClientError::Io));
            try!(buf.trim().parse().map_err(ClientError::ClientId))
        };
        Ok(Client {id: client_id, out: chan, events: events})
    }

    pub fn run(mut self) -> Result<(), ClientError> {
        for event in self.events {
            try!(writeln!(self.out, "{}", event).map_err(ClientError::Io));
        }
        Ok(())
    }
}
