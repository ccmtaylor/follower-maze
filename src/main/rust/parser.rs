use nom::digit;
use std::str;
use std::str::FromStr;

use message;


named!(number <u64>,
       map_res!(map_res!(digit, str::from_utf8), FromStr::from_str)
       );

named!(pub message <message::Message>,
       chain!(seq_no: number
              ~ tag!("|")
              ~ event: event,
              || {message::Message{seq_no: seq_no, event: event}})
       );

named!(pub event <message::Event>,
       alt!(follow | unfollow | broadcast | priv_msg | status)
       );

named!(pipenum<u64>, preceded!(tag!("|"), number));

named!(follow <message::Event>,
       chain!(tag!("F")
              ~ src: pipenum
              ~ dst: pipenum,
              || {message::Event::Follow(src, dst)}));

named!(unfollow <message::Event>,
       chain!(tag!("U")
              ~ src: pipenum
              ~ dst: pipenum,
              || {message::Event::Unfollow(src, dst)}));

named!(priv_msg <message::Event>,
       chain!(tag!("P")
              ~ src: pipenum
              ~ dst: pipenum,
              || {message::Event::PrivMsg(src, dst)}));

named!(broadcast <message::Event>,
       map!(tag!("B"), |_: &[u8]| {message::Event::Broadcast}));

named!(status <message::Event>,
       map!(preceded!(tag!("S"), pipenum), |src: u64| {message::Event::Status(src)} ));
