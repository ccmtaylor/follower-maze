#[macro_use] extern crate nom;

mod client;
mod dispatcher;
mod message;
mod parser;
mod event_listener;

use std::net::{TcpListener, TcpStream};
use std::io;
use std::thread;
use std::sync::mpsc;


fn spawn_client(conn: TcpStream, notify: mpsc::Sender<dispatcher::Message>) -> thread::JoinHandle<Result<(), client::ClientError>> {
    thread::spawn(move || {
        let (tx, rx) = mpsc::channel();
        let client = try!(client::Client::<TcpStream>::new(conn, rx));
        let id = client.id;
        try!(notify.send(dispatcher::Message::ClientConnected(tx, id)).or(Err(client::ClientError::Other)));
        client.run()
    })
}

pub fn main() {

    let dispatcher = dispatcher::Dispatcher::new();

    let server_tx = dispatcher.tx.clone();
    thread::spawn(move ||{
        let server_listener = TcpListener::bind("127.0.0.1:9090").unwrap();
        let (stream, _) = server_listener.accept().unwrap();
        let event_listener = event_listener::EventListener::new(io::BufReader::new(stream), server_tx);
        event_listener.run();
    });

    let clients_tx = dispatcher.tx.clone();
    thread::spawn(move || {
        let client_listener = TcpListener::bind("127.0.0.1:9099").unwrap();
        for client_conn in client_listener.incoming() {
            if let Ok(conn) = client_conn {
                spawn_client(conn, clients_tx.clone());
            } else {
                println!("ERROR accepting connection {:?}", client_conn);
            }
        }
    });

    dispatcher.run();
}
