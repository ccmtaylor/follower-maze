use nom;
use std::io::prelude::*;
use std::collections::binary_heap;
use std::sync::mpsc;


use dispatcher;
use message;
use parser;

pub struct EventListener<S: BufRead> {
    stream: S,
    queue: binary_heap::BinaryHeap<message::Message>,
    expected: u64,
    events: mpsc::Sender<dispatcher::Message>,
}

impl<S: BufRead> EventListener<S> {
    pub fn new(input: S, output: mpsc::Sender<dispatcher::Message>) -> EventListener<S> {
        EventListener{
            stream: input,
            events: output,
            expected: 1,
            queue: binary_heap::BinaryHeap::new(),
        }
    }

    pub fn run(mut self) {
        for line in self.stream.lines() {
            let line = line.unwrap();
            match parser::message(line.as_bytes()) {
                nom::IResult::Done(rest, msg) => {
                    if !rest.is_empty() {
                        println!("WARN: junk at end of message {}: {:?}", msg, rest);
                    }
                    if msg.seq_no >= self.expected {
                        self.queue.push(msg);
                    } else {
                        println!("WARN: ignoring message {}, expected seq_no > {}", msg, self.expected);
                    }
                    loop {
                        match self.queue.peek() {
                            Some(msg) if msg.seq_no != self.expected => break,
                            None => break,
                            _ => {},
                        }
                        let msg = self.queue.pop().unwrap();
                        self.events.send(dispatcher::Message::MessageReceived(msg)).unwrap();
                        self.expected += 1;
                    }
                }
                _ => println!("invalid message: {}", line)
            }
        }
    }
}
