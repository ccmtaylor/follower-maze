package fm

import java.io.InputStream
import java.util.concurrent.BlockingQueue

import scala.io.BufferedSource

class EventListener(val in: InputStream, queue: BlockingQueue[Any]) extends Runnable {
  private val lines = new BufferedSource(in).getLines()

  def run() {
    lines.flatMap(Message.fromString(_)) foreach { message =>
      queue put ReceivedMessage(message)
    }
  }
}
