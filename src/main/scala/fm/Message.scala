package fm


sealed trait Message {
  val seqNo: Int
}
case class Follow(seqNo: Int, from: Int, to: Int) extends Message {
  override def toString = s"$seqNo|F|$from|$to"
}
case class Unfollow(seqNo: Int, from: Int, to: Int) extends Message {
  override def toString = s"$seqNo|U|$from|$to"
}
case class Broadcast(seqNo: Int) extends Message {
  override def toString = s"$seqNo|B"
}
case class PrivMsg(seqNo: Int, from: Int, to: Int) extends Message {
  override def toString = s"$seqNo|P|$from|$to"
}
case class StatusUpdate(seqNo: Int, from: Int) extends Message {
  override def toString = s"$seqNo|S|$from"
}


object Message {
  val bySeqNo: Ordering[Message] = Ordering.by(_.seqNo)

  def unapply(in: String): Option[Message] = in.split('|') match {
    case Array(int(seq), "F", int(from), int(to)) => Some(Follow(seq, from, to))
    case Array(int(seq), "U", int(from), int(to)) => Some(Unfollow(seq, from, to))
    case Array(int(seq), "B") => Some(Broadcast(seq))
    case Array(int(seq), "P", int(from), int(to)) => Some(PrivMsg(seq, from, to))
    case Array(int(seq), "S", int(from)) => Some(StatusUpdate(seq, from))
    case _ => None
  }

  def fromString = unapply _
  // makes parsing the message strings a bit prettier
  object int {
    def unapply(in: String): Option[Int] = try {
      Some(in.toInt)
    } catch {
      case e: NumberFormatException => None
    }
  }
}