package fm

import java.io.{PrintWriter, OutputStreamWriter, BufferedWriter, OutputStream}
import java.nio.charset.Charset
import java.util.concurrent.{LinkedBlockingQueue, BlockingQueue}

class Client(val me: Int, val os: OutputStream) extends Consumer {

  val input = new LinkedBlockingQueue[Any]
  val out = new PrintWriter(new BufferedWriter(new OutputStreamWriter(os, Charset forName "UTF-8")))

  def consume = { case message: Message =>
    out.print(message)
    out.print("\r\n") // println uses system-specific line separator
    out.flush() // make sure we actually send the message. this cost me a couple of hours :|
  }

  override def shutdown(): Unit = out.close()
}
