package fm

import java.io._
import java.net._
import java.util.concurrent._

import scala.annotation.tailrec
import scala.io._



object Main extends App {

  implicit val codec = Codec.UTF8
  private val pool: ExecutorService = Executors.newCachedThreadPool()


  // all incoming events pass through this.
  val firehose: BlockingQueue[Any] = new LinkedBlockingQueue[Any]()

  val clientSS = new ServerSocket(Env.clientListenerPort)

  pool.submit(new Runnable {
    def run() {
      while(true) {
        val sock = clientSS.accept()
        val in = new BufferedReader(new InputStreamReader(sock.getInputStream))
        val clientId = in.readLine().toInt
        val client: Client = new Client(clientId, sock.getOutputStream)
        pool.submit(client)
        firehose.put(NewConnection(client))
      }
    }
  })

  val eventSS = new ServerSocket(Env.eventListenerPort)
  val sock = eventSS.accept()
  private val eventListenerInput: InputStream = sock.getInputStream
  pool.submit(new EventListener(eventListenerInput, firehose))

  val router: Router = new Router(firehose)
  pool.submit(router)

  readLine("Press enter to exit or click the start button to shutdown.")

  eventListenerInput.close()
  eventSS.close()
  clientSS.close()
  router ! PoisonPill // causes the router to shut down the clients
  pool.shutdown()
}
