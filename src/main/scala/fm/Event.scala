package fm

sealed trait Event
case class ReceivedMessage(msg: Message) extends Event
case class NewConnection(client: Client) extends Event
