package fm

trait Config {
  def eventListenerPort: Int  = getInt("eventListenerPort", 9090)
  def clientListenerPort: Int = getInt("clientListenerPort", 9099)

  def getInt(key: String, default: Int): Int = apply(key).map(_.toInt).getOrElse(default)
  def apply(key: String): Option[String]
}

object Env extends Config {
  override def apply(key: String): Option[String] = Option(java.lang.System.getenv(key))
}
