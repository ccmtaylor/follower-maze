package fm

import java.util.concurrent.BlockingQueue

import scala.collection.mutable.{ListBuffer, ArrayBuffer}
import scala.collection.{immutable, mutable}


class Router(val input: BlockingQueue[Any]) extends Consumer {

  val followers = new mutable.HashMap[Int, mutable.Set[Int]] with mutable.MultiMap[Int, Int] {
    override def default(key: Int) = makeSet
  }
  val clients = mutable.Map[Int, Client]()

  var expectedSeqNo = 1

  // lowest seqNo should go first
  val buffer = mutable.PriorityQueue()(Message.bySeqNo.reverse)

  def handleEvent(message: Message) {
    buffer += message
    while(buffer.nonEmpty && buffer.head.seqNo <= expectedSeqNo) {
      val message = buffer.dequeue()
      if (message.seqNo == expectedSeqNo) expectedSeqNo += 1
      message match {
        case Follow(_, from, to) =>
          followers.addBinding(to, from)
          emit(to)
        case Unfollow(_, from, to) =>
          followers.removeBinding(to, from)
        case Broadcast(_) =>
          clients.keys foreach emit
        case PrivMsg(_, from, to) =>
          emit(to)
        case StatusUpdate(_, from) =>
          followers(from) foreach emit
      }

      def emit(clientId: Int) {
        clients.get(clientId) match {
          case Some(client) =>
            // println(s"$message -> $clientId")
            client ! message
          case None =>
            // ignore
        }
      }
    }
  }

  def consume = { case (message: Event) =>
    message match {
      case ReceivedMessage(m) => handleEvent(m)
      case NewConnection(c) => clients(c.me) = c
    }
  }

  override def shutdown(): Unit = clients.values foreach { client =>
    client ! PoisonPill
  }
}
