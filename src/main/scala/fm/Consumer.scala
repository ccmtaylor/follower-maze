package fm

import java.util.concurrent.BlockingQueue

import scala.annotation.tailrec
import scala.reflect.ClassTag

// idea cribbed from Akka
case object PoisonPill

trait Consumer extends Runnable {
  def input: BlockingQueue[Any]
  def consume: PartialFunction[Any, Unit]

  /** called when a message is not handled by consume */
  def deadLetter(msg: Any): Unit = println(s"Unhandled message: $msg")

  /** override this if you need shutdown behaviour */
  def shutdown() {}

  // pretend we're an actor :)
  def !(it: Any): Unit = {
    input.add(it)
  }

  final def run() {
    @tailrec
    def loop(): Unit = input.take() match {
      case PoisonPill => shutdown()
      case msg if consume isDefinedAt msg => consume(msg); loop()
      case unhandled => deadLetter(unhandled); loop()
    }
    loop()
  }
}
