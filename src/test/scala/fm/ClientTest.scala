package fm

import java.io.ByteArrayOutputStream
import java.util.concurrent.{TimeUnit, ExecutorService, Executors}

import org.scalatest.{BeforeAndAfter, FunSpec}

class ClientTest extends FunSpec {

  describe("Client") {
    it("should ouptut serialized messages on the OutputSream in the order they are received") {
      val os = new ByteArrayOutputStream
      val client = new Client(1, os) with ConsumeAll

      val messages = Seq[Message](Follow(1,2,3), Broadcast(3), Unfollow(2,2,3))

      messages foreach (msg => client ! msg)
      client.consumeAll()

      assert(os.toString("UTF-8") == messages.mkString("", "\r\n", "\r\n"))
    }
  }

}
