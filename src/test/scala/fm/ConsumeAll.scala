package fm

/**
 * this is useful for testing Consumers without needing to worry about thread pools
 * and concurrency. Simply mixin this trait on the test instance and call consumeAll
 * to force the consumer to process all pending messages in the queue.
 */
trait ConsumeAll {
  self: Consumer =>
  def consumeAll() {
    while(!self.input.isEmpty) {
      self.consume(self.input.poll())
    }
  }
}
