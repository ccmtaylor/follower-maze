package fm

import java.io.ByteArrayInputStream
import java.nio.charset.Charset
import java.util.concurrent.LinkedBlockingQueue

import org.scalatest.FunSpec

import scala.collection.JavaConverters._

class EventListenerTest extends FunSpec {
  describe("EventListener") {
    it("should emit messages as they come in") {
      val input =
        """
          |666|F|60|50
          |1|U|12|9
          |542532|B
          |43|P|32|56
          |634|S|32
        """.stripMargin
      checkMessages(input)
    }

    it("should ignore malformed messages") {
      val input =
        """
          |666|F|60|50
          |bad message
          |1|U|12|9
          |1|XXX|12|9
          |
          |542532|B
          |542532|B|more
          |43|P|32|56
          |634|S|32
        """.stripMargin
      checkMessages(input)
    }

    def checkMessages(input: String) {
      val in = new ByteArrayInputStream(input.getBytes(Charset forName "UTF-8"))
      val q = new LinkedBlockingQueue[Any]()
      new EventListener(in, q).run()
      val messages: List[ReceivedMessage] = input
        .lines
        .flatMap(Message.fromString(_))
        .map(ReceivedMessage)
        .toList
      val queueContents: List[Any] = q.asScala.toList
      assert(queueContents == messages)
    }


  }
}
