package fm

import java.util.concurrent.LinkedBlockingQueue

import org.scalatest.FunSpec

class RouterTest extends FunSpec {

  describe("Sequence Id handling") {

    val router = new Router(new LinkedBlockingQueue[Any]()) with ConsumeAll

    it("should buffer messages until the expected sequence id arrives") {
      val initialExpected = router.expectedSeqNo
      // we're using Broadcast here because when there is not client connected,
      // no messages get forwarded
      val messages = (initialExpected + 1 to initialExpected + 10) map Broadcast
      messages foreach { msg =>
        router ! ReceivedMessage(msg)
      }
      router.consumeAll()
      assert(router.expectedSeqNo == initialExpected)
      assert(router.buffer.toSet == messages.toSet)

      router ! ReceivedMessage(Broadcast(router.expectedSeqNo))
      router.consumeAll()
      assert(router.expectedSeqNo == initialExpected + messages.size + 1)
      assert(router.buffer.isEmpty)
    }

    it("should process messages with sequence number that is less than expected") {
      val initExpected = router.expectedSeqNo
      router ! ReceivedMessage(Broadcast(initExpected - 5))
      router.consumeAll()
      assert(router.buffer.isEmpty)
      assert(router.expectedSeqNo == initExpected)
    }
  }
}
