package fm

import org.scalatest.FunSpec
import org.scalatest.Matchers._

class MessageTest extends FunSpec {

  describe("the extractor for Messages") {
    it("should work with valid inputs") {
      assertResult(Follow(666, 60, 50)) {
        val Message(follow) = "666|F|60|50"
        follow
      }
      assertResult(Unfollow(1, 12, 9)) {
        Message.fromString("1|U|12|9").get
      }
      assertResult(Broadcast(542532)) {
        Message.fromString("542532|B").get
      }
      assertResult(PrivMsg(43, 32, 56)) {
        Message.fromString("43|P|32|56").get
      }
      assertResult(StatusUpdate(634, 32)) {
        Message.fromString("634|S|32").get
      }
    }

    it("should return None for invalid messages") {
        Message.fromString("foo") shouldBe None
        Message.fromString("0|X|0") shouldBe None
        Message.fromString("666|F|60|50|more") shouldBe None
        Message.fromString("666|B|more") shouldBe None
        Message.fromString("") shouldBe None
    }
  }
}
