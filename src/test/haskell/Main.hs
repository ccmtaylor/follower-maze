module Main ( main ) where

import Message

import Data.Attoparsec.Text
import Test.Tasty
import Test.Tasty.QuickCheck as QC
-- import Test.QuickCheck

instance Arbitrary Message where
  arbitrary = do
    x <- choose (1, 5) :: Gen Int
    seqNo <- arbitrary
    case x of
      1 -> do from <- arbitrary
              to <- arbitrary
              return (Follow seqNo from to)
      2 -> do from <- arbitrary
              to <- arbitrary
              return (Unfollow seqNo from to)
      3 -> return (Broadcast seqNo)
      4 -> do from <- arbitrary
              to <- arbitrary
              return (PrivMsg seqNo from to)
      5 -> do from <- arbitrary
              return (StatusUpdate seqNo from)
              

prop_asString :: Message -> Bool
prop_asString msg = (parseOnly message (asString msg)) == Right msg

main :: IO ()
main = defaultMain tests

tests :: TestTree
tests = testGroup "Tests" [properties, unitTests]

properties :: TestTree
properties = testGroup "Properties" [
  testProperty "asString result is parseable" prop_asString
  ]

unitTests :: TestTree
unitTests = testGroup "Unit Tests" []
