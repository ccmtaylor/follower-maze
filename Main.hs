module Main where

import           Server

import           Control.Concurrent     (forkFinally)
import           Control.Concurrent.STM
import           Control.Exception      (bracket)
import           Control.Monad          (forever)

import           Network
import qualified System.Environment     as Env
import           System.IO
import           Text.Printf            (printf)

serverPort :: IO Int
serverPort = fmap (maybe 9090 read) $
             Env.lookupEnv "eventListenerPort"

clientPort :: IO Int
clientPort = fmap (maybe 9099 read) $
             Env.lookupEnv "clientListenerPort"

talkClient :: Handle -> Server -> IO ()
talkClient h serv = bracket
    (do
      hSetBuffering h LineBuffering
      hSetEncoding h utf8
      uid <- fmap read (hGetLine h)
      putStrLn $ "UserId " ++ (show uid) ++ " connected."
      atomically $ do
        client <- newClient uid h
        addClient serv client
        return client)
    (\client -> do
        printf "client disconnected: %s" (clientId client)
        atomically $ removeClient serv client)
    runClient


main :: IO ()
main = do
  sp <- serverPort
  cp <- clientPort
  server <- newServer
  serverSock <- listenOn (PortNumber (fromIntegral sp))
  clientSock <- listenOn (PortNumber (fromIntegral cp))
  printf "Listening on ports %d, %d\n" sp cp

  (handle, host, port) <- accept serverSock
  hSetBuffering handle LineBuffering
  hSetEncoding handle utf8

  printf "Accepted server connection from %s: %s\n" host (show port)
  forkFinally (runServer handle server) (\_ -> hClose handle)

  forever $ do
    (chandle, chost, cport) <- accept clientSock
    printf "Accepted client connection from %s: %s\n" chost (show cport)
    forkFinally (talkClient chandle server) (\_ -> hClose chandle)

