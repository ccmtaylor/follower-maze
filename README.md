# Follower-Maze

a twitter-like server that distributes messages to clients.

This repository contains three implementations of the follower-maze
challenge; one written in Scala, one in Haskell, and one in Rust.

The Scala version uses only the standard library and tries to follow
the requirements in [the instructions][followermaze].

I implemented the Haskell version just for fun, so I didn't try to
adhere to the requirement to use as few libraries as possible.

The Rust implementation was for fun as well, though the only external
library it uses is [Nom][nom] for parsing the messages.

# Scala Version

## How to run Follower-Maze

* [Install SBT][sbt], if necessary.
* run `sbt run fm.Main` to launch the server. 
  You can control the listening ports via the same environment variables
  as for the client:
  
  - `eventListenerPort` (default: 9090): the port where events are expected.
  - `clientListenerPort` (default: 9099): the port for client connections.

* in a separate terminal, start the clients via `followermaze.sh`,
  as documented in [the instructions][followermaze].
* when the test is over, stop the server with `Ctrl-C` 
  in the server's terminal.
  
## Architecture

Follower-Maze takes some inspiration from the actor model in that
it is split up into several components that communicate asynchronously
by message passing. Since I needed to avoid external libraries, every
"Actor" is implemented as a separate thread running an event loop that 
receives messages via a `BlockingQueue` -- see the `Consumer` trait.

Since components only communicate via messages, the components are free
to use mutable state internally without needing to worry about concurrent 
access. However, this does incur some overhead due to context switches 
between the threads. In a production environment, I'd elect to use a 
"Real" actor library that doesn't have a dedicated thread per actor 
instead.

The main components of the Follower-Maze are as follows:

### Main

The entry point for the application. It sets up a thread pool for the
various "actors" and the firehose event queue.
Next, it starts an event loop to listen for incoming client connections.
It then listens for (the one and only) event socket connection and starts
the EventListener to handle it.
Once the `EventListener` is running, it launches a router to dispatch the 
messages to the client connections.

Main then waits for user input from the console and shuts down everything.
  
### EventListener

The `EventListener` reads from the event socket (port 9090 by default).
It decodes the messages (via `Message.fromString`) as they arrive and
dispatches them to the "firehose" queue.

### Client

The `Main` class starts a `Client` actor for each client connection 
(port 9099). Clients listen for messages on their queue and serialize
them out to the wire.

### Router

This is where the business logic lives. The router listens on the
firehose queue and processes the messages. It manages a map of followers
for each client (modified via `Follow`/`Unfollow` messages).
The firehose also receives a message (from `Main`) whenever a new client 
connects. `Router` keeps a list of connected clients and
dispatches messages to the `Client`s for sendout.


# Haskell Version

As mentioned above, I implemented the Haskell version just for fun,
though it does work against the supplied client with comparable or
better performance than the Scala version.

The architecture is similar to the Scala version in that I also use a
thread for each client and queues for communication between
them. Unlike Scala, I don't use mutable collections, but instead I use
mutable references to immutable data. Concurrent updates are managed
via Software Transactional Memory (STM), and thus should be safe.

The code is loosely based on examples in Simon Marlow's book,
[Parallel and Concurrent Programming in  Haskell][marlow].

## How to run

I use Cabal, the standard Haskell build tool to build the project. In
the root directory, run
```bash
# don't pollute your home directory
cabal sandbox init
# build dependencies
cabal install --only-dependencies
# run the tests
cabal test
# run the server
cabal run
```

Ports can be configured using the same environment variables as in the
Scala version.


# Rust Version

Another implementation following the same pattern, but in Rust. It's about
as fast as the other implementations, but it only uses 17k of RAM while running

## How to running

The project uses the Rust build tool `cargo`. Assuming you have a working Rust
installation (v1.5+), run

```bash
# download dependencies, build and run the application.
cargo run --release
```


[sbt]: http://www.scala-sbt.org/release/tutorial/Setup.html
[followermaze]: instructions.md
[marlow]: http://chimera.labs.oreilly.com/books/1230000000929/ch12.html
[nom]: https://github.com/Geal/nom
